pipeline{
    agent{
        node {
            label 'docker' 
        }
    }
    environment {
        CONTAINER_REGISTRY_URL = 'registry.gitlab.com'
        PROJECT_DIR = """$WORKSPACE"""
    }

    stages{
        stage('Checkout') {
            steps {
                // sh 'printenv'
                script {
                    if ("TAG_PUSH".equals(env.gitlabActionType)) {
                        env.branchName = sh (returnStdout: true, script:"echo \$(basename ${env.gitlabSourceBranch})").trim();
                        env.refspec = "+refs/tags/*:refs/remotes/origin/tags/*";
                    } else {
                        
                        if (env.gitlabSourceBranch == null) {
                            env.branchName = "main";
                        } else {
                            env.branchName = "${env.gitlabSourceBranch}";
                        }
                        
                        env.refspec = "+refs/heads/*:refs/remotes/origin/*";
                    }
                }
                checkout([$class: 'GitSCM', branches: [[name: "${env.branchName}"]], extensions: [], userRemoteConfigs: [[refspec: "${env.refspec}", url: 'https://gitlab.com/opcal-project/container/linux.git']]])
            }
        }

        stage('prepare') {
            steps{
                script {
                    env.TIMESTAMP = sh (returnStdout: true, script:'echo $(date +%y.%m.%d.%H.%M)').trim();
                    env.TAG_VERSION = "${BUILD_NUMBER}";
                    withCredentials([usernamePassword(credentialsId: 'gitlab-containers-registry', passwordVariable: 'CONTAINER_PWD', usernameVariable: 'CONTAINER_USER')]) {
                        sh 'apt update && apt install curl'
                        sh 'docker login ${CONTAINER_REGISTRY_URL} -u ${CONTAINER_USER} -p ${CONTAINER_PWD}'
                        sh 'chmod +x ${WORKSPACE}/builds/**.sh'
                    }
                }
            }
        }
        
        stage('build linux') {
            failFast false
            parallel {
                stage('build alpine') {
                    steps{
                        script {
                            sh '${WORKSPACE}/builds/alpine.sh'
                        }
                    }
                }
                stage('build oraclelinux') {
                    steps{
                        script {
                            sh '${WORKSPACE}/builds/oraclelinux.sh'
                        }
                    }
                }
                stage('build ubuntu') {
                    steps{
                        script {
                            sh '${WORKSPACE}/builds/ubuntu.sh'
                        }
                    }
                }
            }
        }
    }
    post { 
        always { 
          cleanWs()
          script {
                sh 'docker image prune -f'
            }
        }
    }

}