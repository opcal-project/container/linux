#!/bin/sh

set -e

export TIMESTAMP=$(date +%y.%m.%d.%H.%M)

export TAG_VERSION=${BUILD_NUMBER}

echo 'current build tag ${TAG_VERSION}'

echo " "
echo " "
echo 'build linux start'

# alpine
find ${PROJECT_DIR}/alpine/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# oraclelinux
find ${PROJECT_DIR}/oraclelinux/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# ubuntu
find ${PROJECT_DIR}/ubuntu/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

echo 'build linux finished'
echo " "
echo " "